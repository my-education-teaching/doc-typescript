---
outline: "deep"
---

# tsconfig配置项

**作用**：配置 tsc 编译器以什么样的形式去编译和检查代码

**官网**：https://www.typescriptlang.org/tsconfig/

```js
// 开发库常用配置项
{
  "compilerOptions": {
    "module": "esnext",			// 指定生成什么模块代码
    "target": "esnext",			// 设置 JavaScript 语言版本并包含兼容的库声明，会被 lib 覆盖
    "moduleResolution": "node",	// 指定 TypeScript 如何从给定的模块说明符中查找文件
    "esModuleInterop": true,	// 简化对导入 CommonJS 模块的支持
    "strict": true,				// 开启严格模式
    "skipLibCheck": true,		// 跳过类型检查所有 .d.ts 文件
    "noUnusedLocals": true,		// 在未读取局部变量时启用错误报告
    "resolveJsonModule": true,	// 启用导入 .json 文件
    "jsx": "preserve",			// 指定生成的 JSX 代码
    "lib": ["ESNext", "DOM"]	// 描述目标运行时环境
  },
  "exclude": ["**/node_modules/**", "**/dist/**"]	// 将文件或文件目录排除，不进行编译
}
```



## 1、常用编译选项

### 1.1 Projects

1. **target**：设置 JavaScript 语言版本并包含兼容的库声明
2. **[lib](https://www.typescriptlang.org/tsconfig#lib)**：指定一组**描述目标运行时环境（如：dom）**的捆绑库声明文件，**设置 target** 相当于指定了一个**环境预设**。**开启后会覆盖 target 选项**

### 1.2 Modules

1. **module**：指定生成什么模块代码
2. **rootDir**：在源文件中指定根文件夹。

### 1.3 Emit optiones

1. **sourceMap**：生成 ts 文件的映射，再开发复杂 ts 文件时有用
2. **outFile**：指定将所有输出捆绑到一个 JavaScript 文件中的文件。如果 `declaration` 为真，还指定一个捆绑所有 .d.ts 输出的文件。
3. **outDir**：为所有生成的文件指定一个输出文件夹
4. **noEmitOnError**：当代码错误时不生成 js 文件

### 1.4 Type Checking

- **strict**：开启所有严格检查



## 2、其它编译选项

### 2.1 Projects

1. <span style="color:red">**incremental**</span>：启用增量编译

   > TypeScript 3.4 引入了一个名为增量的新标志，它告诉 TypeScript 保存有关上次编译的项目图的信息。下次以增量方式调用 TypeScript 时，它将使用该信息来**检测成本最低的方式来进行类型检查并为您的项目发出更改**。（增量编译**：根据文件的修改时间来判断是否要更新**）

2. **composite**：启用允许 TypeScript 项目与项目引用一起使用的约束

3. **tsBuildInfoFile**：指定 .tsbuildinfo 增量编译文件的文件夹

4. **disableSourceOfProjectReferenceRedirect**：在引用复合项目时禁用首选源文件而不是声明文件

5. **disableSolutionSearching**：编辑时选择项目退出多项目引用检查

6. **disableReferencedProjectLoad**：减少 TypeScript 自动加载的项目数量

### 2.2 Language and Environment

1. <span style="color:red">**target**</span>：设置 JavaScript 语言版本并包含兼容的库声明
2. <span style="color:red">**lib**</span>：指定一组**描述目标运行时环境（如：dom）**的捆绑库声明文件，**设置 target** 相当于指定了一个**环境预设**。**开启后会覆盖 target 选项**，[详述查看](https://www.typescriptlang.org/tsconfig#lib)
3. **jsx**：指定生成的 JSX 代码
4. **experimentalDecorators**：启用对 TC39 第 2 阶段草稿装饰器的实验性支持。
5. **jsxFactory**：指定针对 React JSX 生成时使用的 JSX 工厂函数，例如'React.createElement' 或 'h'
6. **jsxFragmentFactory**：当针对 React JSX 生成时，指定用于片段的 JSX 片段引用，例如“React.Fragment”或“Fragment”。
7. **jsxImportSource**：指定模块说明符，用于在使用 `jsx: react-jsx*` 时导入 JSX 工厂函数。
8. **reactNamespace**：指定为 `createElement` 调用的对象。这仅适用于以 `react` JSX 生成为目标的情况。
9. **noLib**：禁止包含任何库文件，包括默认的 lib.d.ts。
10. **useDefineForClassFields**：发出符合 ECMAScript 标准的类字段。

### 2.3 Modules

1. <span style="color:red">**module**</span>：指定生成什么模块代码
2. <span style="color:red">**rootDir**</span>：在源文件中指定根文件夹
3. **moduleResolution**：指定 TypeScript 如何从给定的模块说明符中查找文件
4. **baseUrl**：指定解析非相对模块名称的基目录
5. **paths**：指定一组将导入重新映射到其他查找位置的条目
6. **rootDirs**：解析模块时允许将多个文件夹视为一个
7. **typeRoots**：指定多个文件夹，其行为类似于`./node_modules/@types`
8. **types**：指定要包含的类型包名称，而不在源文件中引用
9. **allowUmdGlobalAccess**：允许从模块访问 UMD 全局变量
10. **resolveJsonModule**：启用导入 .json 文件
11. **noResolve**：禁止 `import`s、`require`s 或 `<reference>`s 扩展 TypeScript 应添加到项目中的文件数量。

### 2.4 JavaScript Support

1. **allowJs**：允许 JavaScript 文件进行检查。 使用 `checkJS` 选项从这些文件中获取错误。
2. **checkJs**：在类型检查的 JavaScript 文件中启用错误报告。
3. **maxNodeModuleJsDepth**：指定用于从 `node_modules` 检查 JavaScript 文件的最大文件夹深度。 仅适用于 `allowJs`。

### 2.5 Emit

1. <span style="color:red">**declaration**</span>：从项目中的 TypeScript 和 JavaScript 文件生成 .d.ts 文件。
2. <span style="color:red">**declarationMap**</span>：为 d.ts 文件创建源映射。
3. <span style="color:red">**emitDeclarationOnly**</span>：只输出 d.ts 文件，不输出 JavaScript 文件。
4. <span style="color:red">**sourceMap**</span>：生成 ts 文件的映射，再开发复杂 ts 文件时有用
5. <span style="color:red">**outFile**</span>：指定将所有输出捆绑到一个 JavaScript 文件中的文件。如果 `declaration` 为真，还指定一个捆绑所有 .d.ts 输出的文件。
6. <span style="color:red">**outDir**</span>：为所有生成的文件指定一个输出文件夹
7. <span style="color:red">**removeComments**</span>：禁止生成注释，减少文件体积
8. <span style="color:red">**noEmit**</span>：禁止从编译中生成文件，只做检查
9. **importHelpers**：允许每个项目从 tslib 导入帮助函数一次，而不是在每个文件中包含它们。
10. **importsNotUsedAsValues**：为仅用于类型的导入指定发出/检查行为
11. **downlevelIteration**：为迭代发出更兼容但冗长且性能更低的 JavaScript。
12. **sourceRoot**：指定调试器查找参考源代码的根路径。
13. **mapRoot**：指定调试器应该定位地图文件的位置，而不是生成的位置。
14. **inlineSourceMap**：在发出的 JavaScript 中包含源映射文件。
15. **inlineSources**：在发出的 JavaScript 内的源映射中包含源代码。
16. **emitBOM**：在输出文件的开头发出一个 UTF-8 字节顺序标记 (BOM)。
17. <span style="color:red">**newLine**</span>：设置生成文件的换行符。
18. **stripInternal**：禁止发出在其 JSDoc 注释中包含 `@internal` 的声明。
19. **noEmitHelpers**：禁止在编译输出中生成自定义帮助函数，如 `__extends`。
20. <span style="color:red">**noEmitOnError**</span>：如果报告任何类型检查错误，则禁止生成文件
21. **preserveConstEnums**：禁止擦除生成代码中的 `const enum` 声明。
22. <span style="color:red">**declarationDir**</span>：指定生成的声明文件的输出目录。
23. <span style="color:red">**preserveValueImports**</span>：在 JavaScript 输出中保留未使用的导入值，否则这些值将被删除。

### 2.6 Interop Constraints

1. **isolatedModules**：确保每个文件都可以安全地转译，而不依赖于其他导入
2. **allowSyntheticDefaultImports**：当模块没有默认导出时允许'import x from y'。
3. <span style="color:red">**esModuleInterop**</span>：发出额外的 JavaScript 以**简化对导入 CommonJS 模块的支持**。 这会启用 `allowSyntheticDefaultImports` 以实现类型兼容性。
4. **preserveSymlinks**：禁止解析符号链接到它们的真实路径。 这与节点中的相同标志相关。
5. <span style="color:red">**forceConsistentCasingInFileNames**</span>：确保导入中的大小写正确

### 2.7 Type Checking

1. <span style="color:red">**strict**</span>：开启所有严格检查
2. **noImplicitAny**：启用带有隐含 `any` 类型的表达式和声明的错误报告
3. **strictNullChecks**：类型检查时，考虑 `null` 和 `undefined`
4. **strictFunctionTypes**：分配函数时，检查以确保参数和返回值是子类型兼容的
5. **strictBindCallApply**：检查 `bind`、`call` 和 `apply` 方法的参数是否与原始函数匹配
6. **strictPropertyInitialization**：检查在构造函数中声明但未设置的类属性
7. **noImplicitThis**：当 `this` 的类型为 `any` 时启用错误报告
8. **useUnknownInCatchVariables**：将 catch 子句变量键入为 'unknown' 而不是 'any'。
9. **alwaysStrict**：确保总是发出'use strict'。
10. <span style="color:red">**noUnusedLocals**</span>：在未读取局部变量时启用错误报告。
11. **noUnusedParameters**：未读取函数参数时引发错误
12. **exactOptionalPropertyTypes**：解释可选的属性类型，而不是添加 'undefined'。
13. **noImplicitReturns**：为未在函数中显式返回的代码路径启用错误报告。
14. **noFallthroughCasesInSwitch**：在 switch 语句中启用失败案例的错误报告。
15. **noUncheckedIndexedAccess**：在索引签名结果中包含 'undefined
16. **noImplicitOverride**：确保派生类中的覆盖成员被标记为覆盖修饰符
17. **noPropertyAccessFromIndexSignature**：对使用索引类型声明的键强制使用索引访问器
18. **allowUnusedLabels**：禁用未使用标签的错误报告
19. **allowUnreachableCode**：禁用无法访问代码的错误报告

### 2.8 Completeness

1. **skipDefaultLibCheck**：跳过 TypeScript 包含的类型检查 .d.ts 文件
2. <span style="color:red">**skipLibCheck**</span>：跳过对所有 .d.ts 文件的类型检查



## 3、其它非编译选项

1. **[include](https://www.typescriptlang.org/tsconfig#include)**：显示指定编译的文件或者文件目录
2. **[exclude](https://www.typescriptlang.org/tsconfig#exclude)**：将文件或文件目录排除，不进行编译
3. **[files](https://www.typescriptlang.org/tsconfig#files)**：指定要包含的文件列表，找不到会报错
4. **[extends](https://www.typescriptlang.org/tsconfig#extends)**：包含要继承的另一个配置文件的路径。该路径可能使用 Node.js 样式解析。
5. **[references](https://www.typescriptlang.org/docs/handbook/project-references.html)**：项目引用是 TypeScript 3.0 中的一项新功能，它允许您将 TypeScript 程序构造成更小的部分。
6. **[watchOptions](https://www.typescriptlang.org/docs/handbook/configuring-watch.html)**：编译器支持配置如何使用 TypeScript 3.8+ 中的编译器标志以及之前的环境变量来监听文件和目录。
7. **[ts-node](https://typestrong.org/ts-node/docs/configuration/)**：ts-node 支持多种选项，这些选项可以通过`tsconfig.json`、CLI 标志、环境变量或以编程方式指定
8. **compileOnSave**：保存编译（估计只有用vscode插件才有用）
9. **compilerOptions**：配置编译的选项













 

