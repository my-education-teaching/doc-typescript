---
outline: "deep"
---

# tsc CLI

官方文档：https://www.typescriptlang.org/docs/handbook/compiler-options.html



## 1、基础命令

```bash
tsc		# 查找 tsconfig.json， 根据要求编译当前目录下的所有 ts 文件
tsc src/*.ts	# 指定某个目录
tsc index.ts	# 指定某个文件

tsc --watch		# 监听所有 ts 文件，实时编译成 js 文件
tsc --project tsconfig.myself.json	# 切换配置文件

# 指定文件生成 .d.ts 文件，不输出 ts 文件
tsc index.js --allowJs --declaration --emitDeclarationOnly
# 将两个文件合并（按顺序）输出为一个 index.js 文件
tsc app.ts util.ts --target esnext --outfile index.js
```



## 2、配置

### 2.1 CLI 选项

- **--all**：显示所有编译器选项
- **--generateTrace**：生成事件跟踪和类型列表
- **--help**：提供本地信息以获取有关 CLI 的帮助
- **--listFilesOnly**：打印作为编译一部分的文件的名称，然后停止处理
- **--locale**：从 TypeScript 设置消息传递的语言。这不影响提交
- <span style="color:red">**--project**</span>：在给定配置文件的路径或带有“tsconfig.json”的文件夹的情况下编译项目
- **--showConfig**：打印最终配置而不是构建
- **--version**：打印编译器的版本

### 2.2 构建选项

- **--build**：构建一个或多个项目及其依赖项（如果已过期）
- **--clean**：删除所有项目的输出。
- **--dry**：显示将构建的内容（或删除，如果使用“--clean”指定）
- **--force**：构建所有项目，包括那些看起来是最新的。
- **--verbose**：启用详细日志记录。

### 2.3 监听选项

- <span style="color:red">**--excludeDirectories**</span>：从监听进程中删除目录列表。
- <span style="color:red">**--excludeFiles**</span>：从监听模式的处理中删除文件列表。
- **--fallbackPolling**：指定当系统用完本机文件观察程序时观察程序应使用的方法。
- **--synchronousWatchDirectory**：在本机不支持递归观察的平台上同步调用回调并更新目录观察者的状态。
- <span style="color:red">**--watch**</span>：监听输入文件
- **--watchDirectory**：指定如何在缺少递归文件监听功能的系统上监视目录。
- **--watchFile**：指定 TypeScript 监听模式的工作方式。

### 2.4 其它

- <span style="color:red">**--noEmitOnError**</span>：报错时不生成输出文件
- <span style="color:red">**--declaration**</span>：从项目中的 TypeScript 和 JavaScript 文件生成 .d.ts 文件。
- <span style="color:red">**--emitDeclarationOnly**</span>：只输出 d.ts 文件，不输出 JavaScript 文件。
- <span style="color:red">**--target**</span>：为提交的 JavaScript 设置 JavaScript 语言版本并包含兼容的库声明。
- 更多请查看[文档](https://www.typescriptlang.org/docs/handbook/compiler-options.html)
