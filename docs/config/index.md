# 配置文件及CLI命令

TS 配置文件属性大全以及CLI命令使用, 需要了解的有:
1.  [tscCLI](/config/tscCLI.md)
2.  [tsconfig配置项](/config/tsconfig配置项.md)
3.  [项目引用](/config/项目引用.md)
