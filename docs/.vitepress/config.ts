export default {
  title: "TS-Note",
  titleTemplate: false,
  head: [["link", { rel: "icon", href: "/favicon.svg" }]],
  lastUpdated: true,
  lang: "en-US",
  base: "/doc-typescript",
  themeConfig: {
    siteTitle: "TypeScript",
    logo: "/favicon.svg",
    socialLinks: [
      { icon: "github", link: "https://github.com/sishen654/TS-Note" },
    ],
    nav: [
      { text: "基础语法", link: "/base/" },
      { text: "高级编程", link: "/advanced/" },
      { text: "配置文件", link: "/config/" },
      { text: "声明文件", link: "/declaration/" },
      { text: "版本更新", link: "/update/" },
      { text: "TS官网链接🚩", link: "https://www.typescriptlang.org/docs/" },
      {
        text: "技术支持✨",
        items: [
          { text: "VitePress", link: "https://vitepress.vuejs.org/" },
          { text: "Vite", link: "https://vitejs.dev/" },
          { text: "Vue3", link: "https://staging-cn.vuejs.org/" },
        ],
      },
    ],
    sidebar: {
      '/advanced/': [
        {
          text: 'advanced',
          items: [
            { text: '01-实用程序类型', link: '/advanced/01-实用程序类型.md' },
            { text: '02-TypeScript备忘单', link: '/advanced/02-TypeScript备忘单.md' },
            { text: '03-装饰器', link: '/advanced/03-装饰器.md' },
            { text: '04-声明合并', link: '/advanced/04-声明合并.md' },
            { text: '05-enum', link: '/advanced/05-enum.md' },
            { text: '06-迭代器和生成器', link: '/advanced/06-迭代器和生成器.md' },
            { text: '07-JSX', link: '/advanced/07-JSX.md' },
            { text: '08-混合模式', link: '/advanced/08-混合模式.md' },
            { text: '09-ECMAScript模块', link: '/advanced/09-ECMAScript模块.md' },
            { text: '10-TS中的模块', link: '/advanced/10-TS中的模块.md' },
            { text: '11-模块解析', link: '/advanced/11-模块解析.md' },
            { text: '12-命名空间', link: '/advanced/12-命名空间.md' },
            { text: '13-Symbols', link: '/advanced/13-Symbols.md' },
            { text: '14-三斜杠指令', link: '/advanced/14-三斜杠指令.md' },
            { text: '15-类型兼容性', link: '/advanced/15-类型兼容性.md' },
            { text: '16-类型推断', link: '/advanced/16-类型推断.md' },

          ],
          collapsible: true
        }
      ],
      '/base/': [
        {
          text: 'base',
          items: [
            { text: '01-TS简介', link: '/base/01-TS简介.md' },
            { text: '02-基础数据类型', link: '/base/02-基础数据类型.md' },
            { text: '03-类型范围缩小', link: '/base/03-类型范围缩小.md' },
            { text: '04-函数详解', link: '/base/04-函数详解.md' },
            { text: '05-对象类型', link: '/base/05-对象类型.md' },
            { text: '06-类型操作', link: '/base/06-类型操作.md' },
            { text: '07-类', link: '/base/07-类.md' },
            { text: '08-模块', link: '/base/08-模块.md' },

          ],
          collapsible: true
        }
      ],
      '/config/': [
        {
          text: 'config',
          items: [
            { text: 'tscCLI', link: '/config/tscCLI.md' },
            { text: 'tsconfig配置项', link: '/config/tsconfig配置项.md' },
            { text: '项目引用', link: '/config/项目引用.md' },

          ],
          collapsible: true
        }
      ],
      '/declaration/': [
        {
          text: 'declaration',
          items: [
            { text: '01-简介', link: '/declaration/01-简介.md' },
            { text: '02-declare的作用', link: '/declaration/02-declare的作用.md' },
            { text: '03-声明参考', link: '/declaration/03-声明参考.md' },
            { text: '04-库的结构', link: '/declaration/04-库的结构.md' },
            { text: '05-.d.ts模板', link: '/declaration/05-.d.ts模板.md' },
            { text: '06-.d.ts声明位置', link: '/declaration/06-.d.ts声明位置.md' },
            { text: '07-推荐编写', link: '/declaration/07-推荐编写.md' },
            { text: '08-深潜', link: '/declaration/08-深潜.md' },
            { text: '09-发布', link: '/declaration/09-发布.md' },
            { text: '10-下载', link: '/declaration/10-下载.md' },
            { text: '11-声明文件详解', link: '/declaration/11-声明文件详解.md' },

          ],
          collapsible: true
        }
      ],
      '/update/': [
        {
          text: 'update',
          items: [
            { text: '01-4.8', link: '/update/01-4.8.md' },
            { text: '02-4.9', link: '/update/02-4.9.md' },

          ],
          collapsible: true
        }
      ],
    },
    footer: {
      message: "Released under the MulanPSL-1.0 License.",
      copyright: `Copyright © ${new Date().getFullYear()}-present sishen`,
    },
    editLink: {
      pattern: "https://gitee.com/ma_zipeng/TS-Note/tree/master/docs/:path",
      text: "Edit this page on Gitee",
    },
  },
};
