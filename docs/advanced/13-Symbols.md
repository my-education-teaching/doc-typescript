---
outline: "deep"
---

# 13-Symbols

## 1、基础使用

- **介绍**：从 ECMAScript 2015 开始，`symbol`是一种原始数据类型，就像`number`和`string`.

  `symbol`值是通过调用`Symbol`构造函数创建的。

- **创建**：

  ```tsx
  let sym1 = Symbol()
  let sym2 = Symbol('key')
  ```

- **特殊性**：符号是不可变的，并且是唯一的

  ```tsx
  let sym3 = Symbol('key')
  console.log(sym2 === sym3);   // false
  ```

- 就像字符串一样，符号**可以用作对象属性的键**

  ```tsx
  let obj = {
      [sym1]: 'sishen'
  }
  ```

- 也可以与计算属性声明相结合来**声明对象属性和类成员**

  ```tsx
  let age = Symbol('age')
  class Person {
      [age]() {
          return 18
      }
  }
  Person.prototype[age]       // 18
  ```



## 2、unique symbol

- **作用**：将 Symbol 作为唯一的文字类型

- **创建**：`unique symbol`是`symbol`的子类型，仅由调用`Symbol()`、`Symbol.for()`、显式类型注释产生

- **使用**：只允许在`const`声明和`readonly static`属性上使用

- **案例**：为了引用特定的唯一符号，您必须使用`typeof`运算符。**每个对唯一符号的引用都意味着与给定声明相关联的完全唯一标识。**

  ```tsx
  declare const unique: unique symbol;
  
  let sym: unique symbol = Symbol()
  // ! A variable whose type is a 'unique symbol' type must be 'const'.
  
  const sym2: unique symbol = Symbol()    // OK
  
  let sym3: typeof unique = unique
  // Works - refers to a unique symbol, but its identity is tied to 'unique'.
  
  class C {
      static readonly age: unique symbol= Symbol();
  } 
  ```



## 3、内置Symbol

除了用户定义的符号外，还有内置符号。**内置符号用于表示内部语言行为**。

以下是知名符号的列表：

### `Symbol.hasInstance`

一种确定构造函数对象是否将对象识别为构造函数实例之一的方法。由 instanceof 运算符的语义调用。

### `Symbol.isConcatSpreadable`

一个布尔值，指示对象应通过 Array.prototype.concat 展平为其数组元素。

### `Symbol.iterator`

返回对象的默认迭代器的方法。由 for-of 语句的语义调用。

### `Symbol.match`

将正则表达式与字符串匹配的正则表达式方法。`String.prototype.match`由方法调用。

### `Symbol.replace`

替换字符串的匹配子字符串的正则表达式方法。`String.prototype.replace`由方法调用。

### `Symbol.search`

一种正则表达式方法，它返回与正则表达式匹配的字符串中的索引。`String.prototype.search`由方法调用。

### `Symbol.species`

一个函数值属性，它是用于创建派生对象的构造函数。

### `Symbol.split`

一种在与正则表达式匹配的索引处拆分字符串的正则表达式方法。`String.prototype.split`由方法调用。

### `Symbol.toPrimitive`

一种将对象转换为相应原始值的方法。`ToPrimitive`由抽象操作调用。

### `Symbol.toStringTag`

一个字符串值，用于创建对象的默认字符串描述。由内置方法调用`Object.prototype.toString`。

### `Symbol.unscopables`

一个对象，其自身的属性名称是从关联对象的“with”环境绑定中排除的属性名称。





