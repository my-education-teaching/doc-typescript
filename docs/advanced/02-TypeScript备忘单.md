---
outline: "deep"
---

# 02-TypeScript 备忘单

- 日常 TypeScript 代码不同部分的可下载语法参考页面
- 了解有关类、接口、类型和控制流分析的更多信息

## 1、控制分析流

![a](assets/02-TypeScript%E5%A4%87%E5%BF%98%E5%8D%95.assets/a.png)



## 2、接口

![b](assets/02-TypeScript%E5%A4%87%E5%BF%98%E5%8D%95.assets/b.png)



## 3、类型

![c](assets/02-TypeScript%E5%A4%87%E5%BF%98%E5%8D%95.assets/c.png)



## 4、类

![d](assets/02-TypeScript%E5%A4%87%E5%BF%98%E5%8D%95.assets/d.png)

