---
outline: "deep"
---

# 09-ECMAScript 模块

在过去的几年里，Node.js 一直致力于支持运行 ECMAScript 模块 (ESM)。这是一个非常难以支持的特性，因为 Node.js 生态系统的基础是建立在一个名为 CommonJS (CJS) 的不同模块系统上。

两个模块系统之间的互操作带来了巨大的挑战，需要兼顾许多新功能；然而，**Node.js 中对 ESM 的支持现在已经在 Node.js 中实现，尘埃落定。**

这就是 TypeScript 带来两个新模块和 moduleResolution 设置的原因：**node16** 和 **nodeenext**。

```json
{
    "compilerOptions": {
        "module": "nodenext",
    }
}
```



## 1、nodejs支持ESM

Node.js 支持 package.json 中称为 **type** 的新设置。 “类型”可以设置为“`module`”或“`commonjs`”

```tsx
{
    "name": "my-package",
    "type": "module",
    "//": "...",
    "dependencies": {
    }
}
```

### 1.1 nodeJs设置规则

此设置控制`.js`文件被解释为 ES 模块还是 CommonJS 模块，**未设置时默认为 CommonJS**。当一个文件被**认为是一个 ES 模块时**，与 CommonJS 相比，一些**不同的规则**开始发挥作用：

- `import`/`export`语句和顶层`await`可以使用
- 相对导入路径需要完整扩展（例如，我们必须编写`import "./foo.js"`而不是`import "./foo"`）
- 导入的解析可能与 `node_modules` 中的依赖项不同
- 某些类似全局的值，例如`require()`并且`__dirname`不能直接使用
- CommonJS 模块在某些特殊规则下被导入

### 1.2 TS查找方式

当 TypeScript 找到`.ts`、`.tsx`、`.js`或`.jsx`文件时，它会查找 `package.json`以**查看该文件是否是 ES 模块**，并使用它来确定：

- 如何找到该文件导入的其他模块
- 以及如果产生输出如何转换该文件

**编译模式的不同，导致输出的结果也会不同：**

- 当`.ts`文件被编译为 ES 模块时，ECMAScript `import`/`export`语法在`.js`输出中被单独保留
- 当它被编译为 CommonJS 模块时，它将产生与在 module: commonjs 下获得的相同的输出。

**如果解析不同，可能导致错误发生**：

```tsx
// ./foo.ts
export function helper() {
    // ...
}
// ./bar.ts
import { helper } from "./foo"; // only works in CJS
helper();
```

```tsx
// 在 ES 模块中会失败，因为相对导入路径需要使用扩展
// ./bar.ts
import { helper } from "./foo.js"; // works in ESM & CJS
helper();
```

一般来说，**自动导入和路径补全**这样的 TypeScript 工具通常会为你做这件事。

这**也适用于`.d.ts`文件**。当 TypeScript 在包中找到`.d.ts`文件时，是否将其视为 ESM 或 CommonJS 文件取决于包含的包。



## 2、新文件扩展名

有时需要编写与指定类型不同的文件，Node.js 支持两个扩展来帮助解决这个问题：**.mjs 和 .cjs**。 **.mjs 文件始终是 ES 模块，而 .cjs 文件始终是 CommonJS 模块，没有办法覆盖**它们，因此TS也支持这两个新的文件扩展名。

此外，**TypeScript** 还支持两个**新的声明文件扩展名**：**.d.mts 和 .d.cts**。当 TypeScript **为 .mts 和 .cts 生成声明文件时**，它们对应的扩展名将是 **.d.mts 和 .d.cts**。

**使用这些扩展是完全可选的**，但即使您选择不将它们用作主要工作流程的一部分，它们通常也会很有用。



## 3、CommonJS 互操作

Node.js **允许 ES 模块导入 CommonJS 模块**，就好像它们是具有**默认导出**的 ES 模块一样。

```tsx
// @filename: helper.cts
export function helper() {
    console.log("hello world!");
}
 
// @filename: index.mts
import foo from "./helper.cjs";
 
// prints "hello world!"
foo.helper();
```

在某些情况下，Node.js 还会**从 CommonJS 模块中合成命名导出**，这样会更方便。在这些情况下，ES 模块可以使用“命名空间风格”的导入（即`import * as foo from "..."`），或命名导入（即`import { helper } from "..."`）。

```tsx
// @filename: index.mts
import { helper } from "./helper.cjs";
 
// prints "hello world!"
helper();
```

TS并不总是有办法知道这些命名导入是否会被合成，因此需要使用一些**启发式方法：**

```tsx
// 关于互操作的一个特定于 TypeScript 的注释是以下语法：
import foo = require("foo");
```

**在 CommonJS 模块中，这只是一个`require()`调用**，而在 ES 模块中，这个导入[`createRequire`](https://nodejs.org/api/module.html#module_module_createrequire_filename)来实现同样的事情。**这将使代码在浏览器（不支持`require()`）等运行时上的可移植性降低**，但通常对互操作性很有用。反过来，您可以使用以下语法编写上述示例：

```tsx
// @filename: helper.cts
export function helper() {
    console.log("hello world!");
}
 
// @filename: index.mts
import foo = require("./foo.cjs");		// 在浏览器是不被支持的
 
foo.helper()
```

最后，值得注意的是，**从 CJS 模块导入 ESM 文件的唯一方法是使用动态`import()`调用**。这可能会带来挑战，但这是当今 Node.js 中的行为，如果编写的不是浏览器代码，则需要使用该方法去引入对应的包，**编辑器才能读取得到对应的库声明**

您可以[在此处阅读有关 Node.js 中 ESM/CommonJS 互操作的更多信息](https://nodejs.org/api/esm.html#esm_interoperability_with_commonjs)。



## 4、`package.json`导出、导入和自引用

Node.js 支持在 package.json 中**定义入口点的新字段，称为“[exports](https://nodejs.org/api/packages.html#packages_exports)”**。

这个字段是在 package.json 中**定义“main”的一个更强大的替代方法**，并且可以控制你的包的哪些部分暴露给消费者。

 这是一个 package.json，它**支持 CommonJS 和 ESM 的单独入口点**：

```json
// package.json
{
    "name": "my-package",
    "type": "module",
    "exports": {
        ".": {
            // Entry-point for `import "my-package"` in ESM
            "import": "./esm/index.js",
            // Entry-point for `require("my-package") in CJS
            "require": "./commonjs/index.cjs",
        },
    },
    // CJS fall-back for older versions of Node.js
    "main": "./commonjs/index.cjs",
}
```

**查找规则：**

- 使用 TypeScript 的原始节点支持，它会查找一个`"main"`字段，然后查找与该条目对应的声明文件

  例如，如果`"main"`指向`./lib/index.js`，TypeScript 将查找名为`./lib/index.d.ts`. **包作者可以通过指定一个名为`"types"`（例如`"types": "./types/index.d.ts"`）的单独字段来覆盖它。**

- 新的支持与[导入条件](https://nodejs.org/api/packages.html)类似。默认情况下，TypeScript 使用导入条件覆盖相同的规则——如果你`import`**从 ES 模块编写**一个，它会查找`import`字段，而**从 CommonJS 模块，它会查找`require`字段**。如果找到它们，它将查找一个位于同一位置的声明文件。如果你需要为你的类型声明指向不同的位置，你可以添加一个`"types"`导入条件。



















