---
outline: "deep"
---

# 08-混合模式

除了传统的 OO 层次结构之外，另一种从可重用组件构建类的流行方法是**通过组合更简单的部分类来构建它们**。您可能熟悉 Scala 等语言的 mixin 或特征的概念，并且该模式在 JavaScript 社区中也颇为流行。



## 1、基础使用

该模式依赖于使用具有类继承的泛型来扩展基类。TypeScript 最好的 mixin 支持是通过类表达式模式完成的。[您可以在此处](https://justinfagnani.com/2015/12/21/real-mixins-with-javascript-classes/)阅读有关此模式如何在 JavaScript 中工作的更多信息。

1. **创建一个基类**

   ```tsx
   class Person {
       name = 'base'
       age = 18
       constructor(name: string, age: number) {
           this.name = name;
           this.age = age;
       }
   }
   ```

2. **需要一个类型和一个工厂函数，它返回一个扩展基类的类表达式**

   ```tsx
   // 约束传入的值是一个类
   type Constructor = new (...regs: any[]) => {}
   // 混合了一个 role 属性，以及方法 getRole
   function Mixin<C extends Constructor>(Base: C) {
       return class S extends Base {
           private role = 0
           getRole() {
               return this.role;
           }
       }
   }
   ```

3. 创建一个**应用了 mixins 的基类**：

   ```tsx
   let Child = Mixin(Person)
   let c = new Child('小明', 18)       // S {name: '小明', age: 18, role: 0}
   ```



## 2、受约束的 Mixin

在上面的表格中，mixin 没有类的基础知识，这使得很难创建你想要的设计。

为了对此建模，我们需要**修改原始构造函数类型以接受泛型参数**。

```tsx
// 泛型类型更加灵活
type Constructor<T = {}> = new (...regs: any[]) => T
```

这允许创建**仅适用于受约束基类的类**：

```tsx
type Prient = Constructor<{ print(): void }>
```

**创建仅在有特定基础可以构建时才起作用的 mixin**

```tsx
// 混合了一个 role 属性，以及方法 getRole
function Mixin<C extends Prient>(Base: C) {		// 修改约束的类型
    return class S extends Base {
        private role = 0
        getRole() {
            return this.role;
        }
    }
}
let Child = Mixin(Person)
// 类型 "Person" 中缺少属性 "print"，但类型 "{ print(): void; }" 中需要该属性
```



## 3、约束

### 3.1 装饰器和 Mixin

无法使用装饰器去混合类

```tsx
function Mixin(construct: Person) {
    return class S extends Person {
        age = 18
    }
}

@Mixin
class Person {
    name = 'sihen'
}

let p = new Person()
p.age
//  Property 'age' does not exist on type 'Person'
```

### 3.2 静态属性混合

更多的是一个陷阱而不是一个约束。如下例：无法使用类类型给静态成员使用

```tsx
class Person<T> {
    // ! Static members cannot reference class type parameters.
    static className: T
}
```

可以通过**使用函数返回基于泛型不同的类**来解决此问题：

```tsx
function getPerson<T>() {
    class Person {
        static className: T
    }
    return Person
}

class Student extends getPerson<string>() { }

Student.className   // string
```













