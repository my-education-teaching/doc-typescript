---
outline: "deep"
---

# 11-模块解析

参考地址：https://www.typescriptlang.org/docs/handbook/module-resolution.html

*模块解析* 是编译器用来确定导入所指内容的过程。当引入一个模块时，如：`import { a } from "moduleA"` 编译器将尝试定位代表导入模块的文件。

编译器遵循以下两种不同策略之一：

- [Classic](https://www.typescriptlang.org/docs/handbook/module-resolution.html#classic)
- [Node](https://www.typescriptlang.org/docs/handbook/module-resolution.html#node)

**如果这不起作用**并且模块名称是非相对的（在“moduleA”的情况下是非相对的），那么**编译器将尝试定位[环境模块](https://www.typescriptlang.org/docs/handbook/modules.html#ambient-modules)声明**。

**最后，如果编译器无法解析模块，它将记录一个错误**。在这种情况下，错误将类似于错误 `error TS2307: Cannot find module 'moduleA'.`



## 1、相对与非相对模块导入

根据模块引用是相对的还是非相对的，模块导入的解析方式不同。

- **相对导入**是以 `/`、`./` 或 `../` 开头的导入。一些例子包括：
  - `import Entry from "./components/Entry";`
  - `import { DefaultHeaders } from "../constants/http";`
  - `import "/mod";`
- 任何其他导入都被认为**是非相对的**。一些例子包括：
  - `import * as $ from "jquery";`
  - `import { Component } from "@angular/core";`

**相对导入是相对于导入文件解析的，*不能* 解析为环境模块声明**。你应该为你自己的模块使用相对导入，保证在运行时保持它们的相对位置。

非相对导入可以相对于[`baseUrl`](https://www.typescriptlang.org/tsconfig#baseUrl)或通过路径映射来解决，我们将在下面介绍。它们还可以解析为[环境模块声明](https://www.typescriptlang.org/docs/handbook/modules.html#ambient-modules)**。导入任何外部依赖项时使用非相对路径**。



## 2、模块解析策略

有两种可能的模块解析策略：Node 和 Classic。您可以使用 `moduleResolution` 选项来指定模块解析策略。如果未指定，则对于 `--module commonjs` 默认为 `Node` ，否则为 Classic（包括将 module 设置为 amd、system、umd、es2015、esnext 等时）。

> 注意：节点模块解析是 TypeScript 社区中最常用的，推荐用于大多数项目。如果您在 TypeScript 中遇到导入和导出的解析问题，请尝试设置 moduleResolution: "node" 以查看它是否解决了问题。

### 2.1 Classic

这曾经是 TypeScript 的默认解析策略。如今，这种策略主要是为了向后兼容而存在的。

- **相对导入**

  相对导入将相对于导入文件进行解析。所以 `import { b } from "./moduleB" ` 在源文件中 `/root/src/folder/A.ts` 会导致以下查找：

  1. `/root/src/folder/moduleB.ts`
  2. `/root/src/folder/moduleB.d.ts`

- **非相对导入**

  然而，对于非相对模块导入，**编译器从包含导入文件的目录开始沿着目录树向上走**，试图找到匹配的定义文件。跟上面相同的案例，定位策略如下：

  1. `/root/src/folder/moduleB.ts`
  2. `/root/src/folder/moduleB.d.ts`
  3. `/root/src/moduleB.ts`
  4. `/root/src/moduleB.d.ts`
  5. `/root/moduleB.ts`
  6. `/root/moduleB.d.ts`
  7. `/moduleB.ts`
  8. `/moduleB.d.ts`

### 2.2 Node

这种解析策略试图在运行时模仿[Node.js](https://nodejs.org/)的模块解析机制。[Node.js 模块文档](https://nodejs.org/api/modules.html#modules_all_together)中概述了完整的 Node.js 解析算法。

#### 2.2.1 Node.js 如何解析模块

- **相对导入**

  例如，让我们考虑一个位于 `/root/src/moduleA.js` 的文件，其中包含 `import var x = require("./moduleB");` Node.js 按以下顺序解析该导入：

  1. 询问名为 `/root/src/moduleB.js` 的文件是否存在。

  2. 询问文件夹 `/root/src/moduleB` 是否包含名为 `package.json` 的文件，该文件指定了“main”模块。在我们的示例中，如果 Node.js 找到包含 `{ "main": "lib/mainModule.js" }` 的文件 在 `/root/src/moduleB/package.json`中，那么 Node.js 将引用 `/root/src/moduleB/lib/mainModule.js`。

     **（个人解读：查找如果是文件夹，且下面有一个package.json，根据内部的 main 属性指向的文件，去引用这个文件）**

  3. 询问文件夹 `/root/src/moduleB` 是否包含名为 `index.js` 的文件。该文件被隐式视为该文件夹的“main”模块。

  > 您可以在有关[文件模块](https://nodejs.org/api/modules.html#modules_file_modules)和[文件夹模块](https://nodejs.org/api/modules.html#modules_folders_as_modules)的 Node.js 文档中阅读更多相关信息。

- **非相对导入**

  Node 将在名为 node_modules 的特殊文件夹中查找您的模块。 node_modules 文件夹可以与当前文件处于同一级别，或者在目录链中更高。 **Node 将遍历目录链，遍历每个 node_modules 直到找到您尝试加载的模块**。

  按照上面的示例，考虑 `/root/src/moduleA.js` 是否改为使用非相对路径并具有 `import var x = require("moduleB");`。然后，**Node 会尝试将 moduleB 解析到每个位置，直到一个位置起作用**。

  1. `/root/src/node_modules/moduleB.js`

  2. `/root/src/node_modules/moduleB/package.json` (if it specifies a `"main"` property)

  3. `/root/src/node_modules/moduleB/index.js`

      

  4. `/root/node_modules/moduleB.js`

  5. `/root/node_modules/moduleB/package.json` (if it specifies a `"main"` property)

  6. `/root/node_modules/moduleB/index.js`

      

  7. `/node_modules/moduleB.js`

  8. `/node_modules/moduleB/package.json` (if it specifies a `"main"` property)

  9. `/node_modules/moduleB/index.js`

  您可以在 Node.js 文档中阅读有关[从 node_modules 加载模块的更多信息](https://nodejs.org/api/modules.html#modules_loading_from_node_modules_folders)。

#### 2.2.2 TypeScript 如何解析模块

TypeScript 将模仿 Node.js 运行时解析策略，以便在编译时定位模块的定义文件。

为此，TypeScript 将 TypeScript 源文件扩展名 `.ts ` 、`.tsx`、`.d.ts` 覆盖在 Node 的解析逻辑上。 

TypeScript 还将使用 `package.json` 中名为 **types（module | commonjs）** 的字段来反映“main”的目的——编译器将使用它来查找“main”定义文件以进行查阅。

 例如，像 `/root/src/moduleA.ts` 中的 `import { b } from "./moduleB"` 这样的导入语句将导致尝试以下位置来定位 `"./moduleB"`：

- **相对导入**

  1. `/root/src/moduleB.ts`
  2. `/root/src/moduleB.tsx`
  3. `/root/src/moduleB.d.ts`
  4. `/root/src/moduleB/package.json` (if it specifies a `types` property)
  5. `/root/src/moduleB/index.ts`
  6. `/root/src/moduleB/index.tsx`
  7. `/root/src/moduleB/index.d.ts`

- **非相对导入**

  同样，非相对导入将遵循 Node.js 解析逻辑，首先查找文件，然后查找适用的文件夹。所以 `import { b } from "moduleB"` 在源文件中 `/root/src/moduleA.ts` 将导致以下查找：

  1. `/root/src/node_modules/moduleB.ts`

  2. `/root/src/node_modules/moduleB.tsx`

  3. `/root/src/node_modules/moduleB.d.ts`

  4. `/root/src/node_modules/moduleB/package.json` (if it specifies a `types` property)

  5. `/root/src/node_modules/@types/moduleB.d.ts`

  6. `/root/src/node_modules/moduleB/index.ts`

  7. `/root/src/node_modules/moduleB/index.tsx`

  8. `/root/src/node_modules/moduleB/index.d.ts`

      

  9. `/root/node_modules/moduleB.ts`

  10. `/root/node_modules/moduleB.tsx`

  11. `/root/node_modules/moduleB.d.ts`

  12. `/root/node_modules/moduleB/package.json` (if it specifies a `types` property)

  13. `/root/node_modules/@types/moduleB.d.ts`

  14. `/root/node_modules/moduleB/index.ts`

  15. `/root/node_modules/moduleB/index.tsx`

  16. `/root/node_modules/moduleB/index.d.ts`

        

  17. `/node_modules/moduleB.ts`

  18. `/node_modules/moduleB.tsx`

  19. `/node_modules/moduleB.d.ts`

  20. `/node_modules/moduleB/package.json` (if it specifies a `types` property)

  21. `/node_modules/@types/moduleB.d.ts`

  22. `/node_modules/moduleB/index.ts`

  23. `/node_modules/moduleB/index.tsx`

  24. `/node_modules/moduleB/index.d.ts`

  

  不要被这里的步骤数量吓倒 - TypeScript 仍然只在步骤 (9) 和 (17) 中两次跳转目录。这实际上并不比 Node.js 本身所做的复杂。



## 3、附加模块解析标志

项目源布局有时与输出的布局不匹配。通常一组构建步骤会生成最终输出。其中包括将 .ts 文件编译为 .js，以及将依赖项从不同的源位置复制到单个输出位置。

最终结果是运行时的模块可能与包含其定义的源文件**具有不同的名称**。或者最终输出中的模块路径在编译时可能与其对应的**源文件路径不匹配**。

 **TypeScript 编译器有一组附加标志**，用于**通知编译器预期发生在源上的转换以生成最终输出**。 

重要的是要注意编译器不会执行任何这些转换；它只是使用这些信息来**指导将模块导入解析为其定义文件的过程**。

### 3.1 Base URL

在使用 AMD 模块加载器的应用程序中，使用 [baseUrl](https://www.typescriptlang.org/tsconfig/#baseUrl) 是一种常见做法，其中模块在运行时“部署”到单个文件夹。这些模块的源代码可以位于不同的目录中，但是**构建脚本会将它们放在一起**。

设置 baseUrl 会通知编译器在哪里可以找到模块。假定所有具有非相对名称的模块导入都与 baseUrl 相关。 baseUrl 的值被确定为：

- baseUrl 命令行参数的值（如果给定路径是相对的，则根据当前目录计算）
- 'tsconfig.json' 中 baseUrl 属性的值（如果给定路径是相对的，则根据 'tsconfig.json' 的位置计算）

请注意，**相对模块导入不受设置 baseUrl 的影响**，因为它们总是相对于它们的导入文件进行解析。

您可以在 [RequireJS](https://requirejs.org/docs/api.html#config-baseUrl) 和 [SystemJS](https://github.com/systemjs/systemjs/blob/main/docs/api.md) 文档中找到有关 baseUrl 的更多文档。

### 3.2 Path mapping

有时模块不直接位于 baseUrl 下。例如，对模块“jquery”的导入将在运行时转换为`“node_modules/jquery/dist/jquery.slim.min.js”`**。加载器使用映射配置在运行时将模块名称映射到文件**，请参阅 [RequireJs](https://requirejs.org/docs/api.html#config-paths) 文档和 [SystemJS](https://github.com/systemjs/systemjs/blob/main/docs/import-maps.md) 文档。

TypeScript 编译器**支持使用 tsconfig.json 文件中的 paths 属性声明此类映射**。这是一个如何为 jquery 指定路径属性的示例：

```tsx
{
  "compilerOptions": {
    "baseUrl": ".", // This must be specified if "paths" is.
    "paths": {
      "jquery": ["node_modules/jquery/dist/jquery"] // This mapping is relative to "baseUrl"
    }
  }
}
```

**请注意，[`paths`](https://www.typescriptlang.org/tsconfig#paths)相对于[`baseUrl`](https://www.typescriptlang.org/tsconfig#baseUrl)解析的**。将 baseUrl 设置为“`.`”以外的值时，即 tsconfig.json 的目录，必须相应地更改映射。比如说，你在上面的例子中设置了`“baseUrl”：“./src”`，那么 jquery 应该映射到`“../node_modules/jquery/dist/jquery”`。

**使用[`paths`](https://www.typescriptlang.org/tsconfig#paths)还允许更复杂的映射，包括多个后备位置**。考虑一个项目配置，其中只有一些模块在一个位置可用，其余模块在另一个位置。构建步骤会将它们放在一个地方。项目布局可能如下所示：

```
projectRoot
├── folder1
│   ├── file1.ts (imports 'folder1/file2' and 'folder2/file3')
│   └── file2.ts
├── generated
│   ├── folder1
│   └── folder2
│       └── file3.ts
└── tsconfig.json
```

对应的`tsconfig.json`看起来像：

```json
{
  "compilerOptions": {
    "baseUrl": ".",
    "paths": {
      "*": ["*", "generated/*"]
    }
  }
}
```

对应的 file1.ts 如下：

```tsx
import 'folder1/file2'		// OK
import 'folder2/file3'		// OK
```

这告诉编译器对于匹配模式“*”（即所有值）的任何模块导入，**在两个位置查找：**

- `“*”`：表示同名不变，所以映射`<moduleName> => <baseUrl>/<moduleName>`
- `"generated/*"` ：表示**带有附加前缀 "generated" 的模块名称**，因此映射 `<moduleName> => <baseUrl>/generated/<moduleName>`

按照这个逻辑，编译器将尝试解析这两个导入：

- `import ‘folder1/file2’`：
  1. **模式 '*' 匹配，通配符捕获整个模块名称**
  2. 尝试列表中的第一个替换：`'*' -> folder1/file2`
  3. 替换的**结果是非相对名称** - 将其与 `baseUrl -> projectRoot/folder1/file2.ts` 结合使用。
  4. 文件已存在。完毕。
- `import ‘folder2/file3’`：
  1. 模式 '*' 匹配，通配符捕获整个模块名称
  2. 尝试列表中的第一个替换：`'*' -> folder2/file3`
  3. 替换的结果是**非相对名称** - 将其与 `baseUrl -> projectRoot/folder2/file3.ts` 结合使用。
  4. 文件不存在，移动到第二个替换  `‘generated/*’ -> generated/folder2/file3`
  5. 替换的结果是非相对名称 - 将其与 `baseUrl -> projectRoot/generated/folder2/file3.ts` 结合使用。
  6. 文件已存在。完毕。

### 3.3 虚拟目录 rootDirs

有时，在编译时来自多个目录的项目源全部组合在一起以生成单个输出目录。这**可以看作是一组源目录创建了一个“虚拟”目录**。

使用 rootDirs，您可以通知编译器组成这个“虚拟”目录的根；因此**编译器可以解析这些“虚拟”目录中的相关模块导入，就好像它们被合并到一个目录中一样**。

例如考虑这个项目结构：

```tsx
 src
 └── views
     └── view1.ts (imports './template1')
     └── view2.ts

 generated
 └── templates
         └── views
             └── template1.ts (imports './view2')
```

要指定与编译器的这种关系，请使用 rootDirs。 **rootDirs 指定一个根列表，其内容预计在运行时合并**。因此，按照我们的示例，tsconfig.json 文件应如下所示：

```json
{
  "compilerOptions": {
    "rootDirs": ["src/views", "generated/templates/views"]
  }
}
```

每次编译器在 [rootDirs](https://www.typescriptlang.org/tsconfig#rootDirs) 的一个子文件夹中看到相对模块导入时，它都会尝试在 [rootDirs](https://www.typescriptlang.org/tsconfig#rootDirs) 的每个条目中查找此导入。

 [rootDirs](https://www.typescriptlang.org/tsconfig#rootDirs) 的灵活性不限于指定逻辑合并的物理源目录列表。**提供的数组可以包含任意数量的临时、任意目录名称，无论它们是否存在**。这**允许编译器以类型安全的方式捕获复杂的捆绑和运行时功能**，例如条件包含和项目特定的加载器插件。

考虑一个国际化场景，其中**构建工具通过插入一个特殊的路径标记（例如 #{locale}）自动生成特定于语言环境的包，作为相对模块路径的一部分**，例如 ./#{locale}/messages。在这个假设设置中，该工具枚举支持的语言环境，将抽象路径映射到 ./zh/messages、./de/messages 等。

假设这些模块中的每一个都导出一个字符串数组。例如 `./zh/messages` 可能包含：

```tsx
export default ["您好吗", "很高兴认识你"];
```

通过利用 `rootDirs`，我们可以**通知编译器这个映射，从而允许它安全地解析 `./#{locale}/messages`**，即使该目录永远不会存在。例如，使用以下 tsconfig.json：

```json
{
  "compilerOptions": {
    "rootDirs": ["src/zh", "src/de", "src/#{locale}"]
  }
}
```

编译器现在将解析来自 `'./#{locale}/messages'` 的导入消息以从 `'./zh/messages'` 导入消息以用于工具目的，**从而允许在不影响设计时间支持的情况下以与语言环境无关的方式进行开发**。



## 4、跟踪模块解析

如前所述，编译器在解析模块时可以访问当前文件夹之外的文件。在诊断模块未解析或解析为不正确定义的原因时，这可能很困难。**启用编译器模块解析跟踪**使用[`traceResolution`](https://www.typescriptlang.org/tsconfig#traceResolution)可以**深入了解模块解析过程中发生的情况**。

假设我们有一个使用 `typescript` 模块的示例应用程序。 `app.ts` 有一个 `import * as ts from "typescript"` 之类的导入。

```
│   tsconfig.json
├───node_modules
│   └───typescript
│       └───lib
│               typescript.d.ts
└───src
        app.ts
```

使用 [traceResolution](https://www.typescriptlang.org/tsconfig#traceResolution) 调用编译器

```bash
tsc --traceResolution
```

结果输出如下：

```tsx
======== Resolving module 'typescript' from 'src/app.ts'. ========
Module resolution kind is not specified, using 'NodeJs'.
Loading module 'typescript' from 'node_modules' folder.
File 'src/node_modules/typescript.ts' does not exist.
File 'src/node_modules/typescript.tsx' does not exist.
File 'src/node_modules/typescript.d.ts' does not exist.
File 'src/node_modules/typescript/package.json' does not exist.
File 'node_modules/typescript.ts' does not exist.
File 'node_modules/typescript.tsx' does not exist.
File 'node_modules/typescript.d.ts' does not exist.
Found 'package.json' at 'node_modules/typescript/package.json'.
'package.json' has 'types' field './lib/typescript.d.ts' that references 'node_modules/typescript/lib/typescript.d.ts'.
File 'node_modules/typescript/lib/typescript.d.ts' exist - use it as a module resolution result.
======== Module name 'typescript' was successfully resolved to 'node_modules/typescript/lib/typescript.d.ts'. ========
```

**需要注意的事项**

- 导入名称和位置

  ```bash
  ======== Resolving module ‘typescript’ from ‘src/app.ts’. ========
  ```

- 编译器遵循的策略

  ```bash
  Module resolution kind is not specified, using ‘NodeJs’.
  ```

- 从 npm 包中加载类型

  > package.json 有 **types** 字段
  >
  > `./lib/typescript.d.ts` 引用 `‘node_modules/typescript/lib/typescript.d.ts’`。

- 最后结果

  ```bash
  ======== Module name ‘typescript’ was successfully resolved to ‘node_modules/typescript/lib/typescript.d.ts’. ========
  ```

  

## 5、使用`--noResolve`

通常，**编译器会在开始编译过程之前尝试解析所有模块导入**。每次成功解析对文件的导入时，该文件都会添加到编译器稍后将处理的文件集中。

[noResolve](https://www.typescriptlang.org/tsconfig#noResolve) 编译器选项**指示编译器不要将任何未在命令行上传递的文件“添加”到编译中**。它仍会尝试将模块解析为文件，但**如果未指定文件，则不会包含该文件**。

例如：

```tsx
import * as A from "moduleA"; // OK, 'moduleA' passed on the command-line
import * as B from "moduleB"; // Error TS2307: Cannot find module 'moduleB'.
```

```bash
tsc app.ts moduleA.ts --noResolve
```

编译`app.ts`使用 [`noResolve`](https://www.typescriptlang.org/tsconfig#noResolve) 应该导致：

- 正确查找`moduleA`，因为它是在命令行上传递的。
- 由于未通过而未找到错误`moduleB`。



## 6、常见问题

**为什么排除列表中的模块仍然被编译器拾取？**

- tsconfig.json 将一个文件夹变成一个“项目”。在不指定任何`“exclude”`或`“files”`条目的情况下，包含 tsconfig.json 的文件夹中的所有文件及其所有子目录都将包含在您的编译中。**如果要排除某些文件，请使用“exclude”，如果您希望指定所有文件而不是让编译器查找它们，请使用“files”**。
- 那是 `tsconfig.json` 自动包含。如上所述，这并没有嵌入模块解析。**如果编译器将文件识别为模块导入的目标**，则无论它是否在前面的步骤中被排除，它将被包含在编译中。 
- 因此，要从编译中排除一个文件，您需要排除它以及所有具有 `import` 或 `/// <reference path="..." />` 指令的文件。















