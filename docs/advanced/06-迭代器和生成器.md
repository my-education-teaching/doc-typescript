---
outline: "deep"
---

# 06-迭代器和生成器

## 1、可迭代对象

如果对象具有 Symbol.iterator 属性的实现，则该对象被认为是可迭代的。一些内置类型，如 **Array、Map、Set、String、Int32Array、Uint32Array** 等，它们的 Symbol.iterator 属性已经实现。

**对象上的 Symbol.iterator 函数负责返回要迭代的值列表**。

### 1.1 `Iterable` interface

如果我们想采用上面列出的可迭代类型，Iterable 是我们可以使用的类型

```tsx
function toArray<X>(xs: Iterable<X>): X[] {
    return [...xs]
}
```

### 1.2 for...of 语句

- **作用：**循环遍历一个可迭代对象，调用该`Symbol.iterator`对象的属性。

- **使用**

  ```tsx
  let someArray = [1, "string", false];
  for (let entry of someArray) {
    console.log(entry); // 1, "string", false
  }
  ```

### 1.3 for...of 与 for...in 语句

都表示迭代列表，但是**迭代的值是不同的**。`for..in`返回被迭代对象的***键*** 列表，而`for..of`返回被迭代对象的数字属性的***值*** 列表。

```tsx
let list = [4, 5, 6];
for (let i in list) {
  console.log(i); // "0", "1", "2",
}
for (let i of list) {
  console.log(i); // 4, 5, 6
}
```



## 2、代码生成

### 2.1 针对 ES5 和 ES3

当以 ES5 或 ES3 兼容引擎为目标时，迭代器**只允许在`Array`类型值上**。对非数组值使用循环是错误的`for..of`，即使这些非数组值实现了该`Symbol.iterator`属性。

**编译器将为 for..of 循环生成一个简单的 for 循环**，例如：

```tsx
// ts代码
let numbers = [1, 2, 3];
for (let num of numbers) {
  console.log(num);
}
```

```tsx
// 生成代码
var numbers = [1, 2, 3];
for (var _i = 0; _i < numbers.length; _i++) {
  var num = numbers[_i];
  console.log(num);
}
```

### 2.2 以 ECMAScript 2015 及更高版本为目标

当以符合 ECMAScipt 2015 的引擎为目标时，编译器将生成 for..of 循环**以针对引擎中的内置迭代器实现**。







