---
outline: "deep"
---

# 05-enum

枚举是 TypeScript 的少数功能之一，它不是 JavaScript 的类型级扩展。

枚举允许开发人员**定义一组命名常量**。使用枚举可以更轻松地记录意图，或创建一组不同的案例。TypeScript 提供基于数字和基于字符串的枚举。



## 1、数字枚举

**定义了数字，往后的数字会自增，不添加的话，默认初始是0**

```tsx
enum Direction {
    Up = 1,
    Down,   // 2
    Left,   // 3
    Right,  // 4
}
```

**只能使用数字常量作为初始化器**，否则会**报错**

```tsx
function getNum() { return 1 }
enum Direction {
    // 后面不能跟其它值了
    Up = getNum(),
    Up = 'size'.length,
    // Enum member must have initializer.
    Down,
    Left,
    Right,
}
```



## 2、字符串枚举

字符串枚举是一个类似的概念，但有一些细微的运行时差异，如下所述。**在字符串枚举中，每个成员都必须使用字符串文字或另一个字符串枚举成员进行常量初始化。**

```tsx
enum Direction {
    Up = "UP",
    Down = "DOWN",
    Left = "LEFT",
    Right = "RIGHT",
}
```

虽然字符串枚举没有自动递增的行为，**但字符串枚举的好处是它们可以很好地“序列化”**。字符串枚举**允许您在代码运行时提供有意义且可读的值，而与枚举成员本身的名称无关**。



## 3、异构枚举

从技术上讲，**枚举可以与字符串和数字成员混合**，但不清楚你为什么要这样做：

```tsx
enum BooleanLikeHeterogeneousEnum {
  No = 0,
  Yes = "YES",
}
```

除非您真的想以一种**巧妙的方式利用** JavaScript 的运行时行为，**否则建议您不要这样做**。



## 4、计算成员和常量成员

每个枚举成员都有一个与之关联的值，可以是*常量* 或*计算* 值。

### 4.1 常量成员

在以下情况下，枚举成员被认为是常量：

- 它是枚举中的**第一个成员，并且没有初始化程序**，在这种情况下，它被分配了值`0`：

  ```tsx
  enum E {
    X,
  }
  ```

- **它没有初始化程序，并且前面的枚举成员是一个*数字* 常量**。在这种情况下，当前枚举成员的值将是前一个枚举成员的值加一。

  ```tsx
  enum E2 {
    A = 1,
    B,
    C,
  }
  ```

- 枚举成员使用常量枚举表达式进行初始化。常量枚举表达式是可以在编译时完全评估的 TypeScript 表达式的子集。一个表达式是一个常量枚举表达式，如果它是：

  1. **文字枚举表达式**（基本上是字符串文字或数字文字）
  2. **对先前定义的常量枚举成员的引用**（可以源自不同的枚举）
  3. **带括号**的常量枚举表达式
  4. 应用于常量枚举表达式的`+`, `-`,**一元运算符**之一`~`
  5. `+`, `-`, `*`, `/`, `%`, `<<`, `>>`, `>>>`, `&`, `|`,`^`以**常量枚举表达式**作为操作数的二元运算符

  将常量枚举表达式计算为`NaN`or是编译时错误`Infinity`。

  ```tsx
  enum FileAccess {
    // constant members
    None,
    Read = 1 << 1,
    Write = 1 << 2,
    ReadWrite = Read | Write,
  }
  ```

### 4.2 计算成员

在所有其他情况下，枚举成员被认为是计算的。

```tsx
enum FileAccess {
  G = "123".length,		// 计算成员
}
```



## 5、联合枚举和枚举成员类型

有一个特殊的未计算的常量枚举成员子集：**文字枚举成员**。文字枚举成员是没有初始化值的**常量枚举成员**，或者具有初始化为的值

- 任何**字符串文字**（例如`"foo"`, `"bar`, `"baz"`）
- 任何**数字文字**（例如`1`, `100`）
- 应用于任何数字文字的**一元减号**（例如`-1`, `-100`）

当枚举中的所有成员都具有文字枚举值时，一些**特殊的语义**就会发挥作用：

- **枚举成员也变成了类型**。例如，我们可以说**某些成员*只能* 具有枚举成员的值**：

  ```tsx
  enum Judge {
      yes,
      no
  }
  
  interface Person {
      sayYes: Judge.yes;
      sayNo: Judge.no;
  }
  
  let p: Person = {
      sayYes: 1,
      sayNo: 'no'
      // ! Type 'string' is not assignable to type 'Judge.no'
  }
  ```

- **使用联合枚举**，类型系统能够利用它**知道枚举本身中存在的确切值集**的事实

  ```tsx
  enum Judge {
      yes,
      no
  }
  
  interface Person {
      say: Judge;
  }
  
  function judge(p: Person) {
      if (p.say === Judge.yes) {		// 作为判断条件
      } else {
      }
  }
  
  judge({ say: 0 })
  ```



## 6、运行时的枚举

枚举是**运行时存在的真实对象**。例如，以下枚举

```tsx
enum E {
  X,
  Y,
  Z,
}
 
function f(obj: { X: number }) {
  return obj.X;
}
 
// Works, since 'E' has a property named 'X' which is a number.
f(E);
```



## 7、编译时的枚举

尽管枚举是运行时存在的真实对象，但 keyof 关键字的工作方式与您对典型对象的预期不同。相反，**使用 `keyof typeof` 来获取将所有 Enum 键表示为字符串的 Type**。

```tsx
enum Judge {
    yes,
    no
}

type Transfrom = keyof typeof Judge
// type Transfrom = "yes" | "no"
```

### 7.1 反向映射

除了为成员创建具有属性名称的对象外，**数字枚举成员还获得从枚举值到枚举名称的*反向映射***

```tsx
enum Judge {
    yes,
    no
}
Judge[0]        // yes
Judge['yes']    // 0
```

**注：字符串枚举成员 *根本不会* 生成反向映射**

```tsx
enum Judge {
    str = 'string'
}
Judge['str']    // string
Judge[0]        // Property '0' does not exist on type 'typeof Judge'
Judge['string']
// Element implicitly has an 'any' type because index expression is not of type 'number'.
```

### 7.2 `const`枚举

**常量枚举只能使用常量枚举表达式**，并且与常规枚举不同，**它们在编译期间会被完全删除**。常量枚举成员在使用**站点内联**。这是可能的，因为 **const 枚举不能有计算成员且无法直接调用枚举名**。

```tsx
const enum Judge {
    yes,
    no,
}
let value = Judge['yes']        // OK
let value2 = Judge[1]
// ! A const enum member can only be accessed using a string literal
console.log(Judge)
// ! "const" 枚举仅可在属性、索引访问表达式、导入声明的右侧、导出分配或类型查询中使用。
```

```tsx
// 生成代码
(()=>{"use strict";console.log(0)})();
```

> 常量枚举陷阱：https://www.typescriptlang.org/docs/handbook/enums.html#const-enum-pitfalls



## 8、环境枚举

环境枚举用于**描述已经存在的枚举类型的形状**。

```tsx
declare enum Enum {
  A = 1,
  B,
  C = 2,
}
let value2 = Enum['A']
console.log(value2);
```

```js
// 编译后执行会报错
(()=>{"use strict";let e=Enum.A;console.log(e)})();
```

**环境枚举和非环境枚举之间的一个重要区别是**，在常规枚举中，如果之前的枚举成员被认为是常量，那么没有初始化器的成员将被认为是常量。相比之下，**没有初始值设定项的环境（和非常量）枚举成员*始终* 被视为已计算。**



## 9、对象与枚举

`as const`在现代 TypeScript 中，**当一个对象足够时，您可能不需要枚举**：

支持这种格式而不是 TypeScript 枚举的**最大论据是它使您的代码库与 JavaScript 的状态保持一致**，并且当 [when/if](https://github.com/rbuckton/proposal-enum) 将枚举添加到 JavaScript 中时，您可以转向其他语法。

```tsx
const enum EDirection {
  Up,
  Down,
  Left,
  Right,
}
 
const ODirection = {
  Up: 0,
  Down: 1,
  Left: 2,
  Right: 3,
} as const;

// 两者作用相同
EDirection.Up;  // Up = 0
ODirection.Up;  // Up: 0
// 作为参数使用
type Direction = typeof ODirection[keyof typeof ODirection];
// type Direction = 0 | 1 | 2 | 3
```













