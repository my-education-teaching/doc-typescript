---
layout: home

hero:
  name: TypeScript
  text: Vite & Vue powered
  tagline: 比较全面的 TS 文档
  image:
    src: /favicon.svg
    alt: VitePress
  actions:
    - theme: brand
      text: 开始学习
      link: /base/
    - theme: alt
      text: 在Gitee中查看
      link: https://gitee.com/ma_zipeng/TS-Note

features:
  - icon: ⚡️
    title: Simple
    details: 简单的案例了解。
  - icon: 🖖
    title: Comprehensive
    details: 全面覆盖 TS 内容。
  - icon: 🛠️
    title: Efficient
    details: 高效率学习和查找。
---

<script setup>
import Record from "./record.vue"
</script>

<Record />
