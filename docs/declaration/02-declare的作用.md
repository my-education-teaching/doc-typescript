---
outline: "deep"
---

# 02-declare的作用

- **作用**：在 .d.ts 的文件中，声明一个全局的类型、变量或者模块等，**即声明一个已经存在的变量的形状**

- **参考**：https://www.bilibili.com/read/cv16886870

- **使用**

  - 比如开发微信网页或小程序时，你也许需要引入微信提供的 JS-SDK，这个 JS-SDK 通常是一个 JavaScript 文件，当通过 script 标签引入之后，经过鉴权验证即可使用微信提供给你的原生功能。但是在 ts 文件中调用全局对象 `wx.xxx` 时会报错

    <img src=".\assets\02-declare的作用.assets\image-20220805134209162-16596782681782.png"/>

    解决方法是定义一个**全局对象**：

    ```tsx
    declare var wx:any
    // 这样，TypeScript 就可以识别全局的 wx 变量了
    ```



## 1、TS内置声明

么在 TypeScript 可以正常使用 JSON、Math、Object，这些全局对象而编译器不会报错的原因在于：**TypeScript 内部帮我们完成了全局声明的操作**

<img src=".\assets\02-declare的作用.assets\image-20220805134225210-16596782681771.png"/>





## 2、类型声明文件

为了更希望准确的定义 wx 的类型，从而得到更加智能的提示，减少错误的调用。 可以**创建一个 wx.d.ts 类型声明文件，并详细声明微信 JS-SDK 提供的方法**。

```tsx
// wx.d.ts
declare namespace wx {
    function getX(s: string): string;
    function getY(s: string): string;
    let legth: 2;
    interface A {
        name: string
    }
}
```

```tsx
// 调用
wx.getX()			// 调用属性
type A = wx.A		// 声明类型
```

值得庆幸的是，常见的 JavaScript 库都已经有官方的类型声明文件，不用自己去定义



## 3、声明文件查找

**查找网址**：https://www.typescriptlang.org/dt/search?search=



## 4、声明模块代码

以 vite 为例，当打开 vite 的声明文件时，可以看到很多声明模块的代码：

<img src=".\assets\02-declare的作用.assets\image-20220805134248284-16596782681784.png"/>

从上面代码可以看到声明了 css、jpg、ttf 模块，**为什么要这样做呢**？

因为**如果不声明的话，TypeScript 的编译器将不会识别这些文件的引入**，并提示出错误信息。比如：

<img src=".\assets\02-declare的作用.assets\image-20220805134258894-16596782681783.png"/>

**通过采用 `*.后缀` 的通配符的形式进行声明**，这是 TypeScript 2.0开始支持的特性。 

<img src=".\assets\02-declare的作用.assets\image-20220805134310170-16596782681785.png"/>



## 5、语法扩展已有代码

假设你想给 Vue 项目引入 axios 作为发送请求的库，你也许想让组件实例自带一个 $axios 属性，直接就能在组件内部使用。 

1. 先进行**类型扩展**，然后再给 config 对象的全局属性对象追加 $axios

   <img src=".\assets\02-declare的作用.assets\image-20220805134327639-16596782681786.png"/>

2. 最后就可以在每个组件实例中的 proxy 对象上使用 $axios 对象来发送请求了

   <img src=".\assets\02-declare的作用.assets\image-20220805134338326-16596782681787.png"/>





