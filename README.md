# TS-Note

#### 介绍

TS 4.7 版本学习笔记

#### 网址查看：http://120.48.46.59:8088/

个人部署商用需要授权.

#### 页面展示

<img src="./imgs/show.png">

#### 自动化脚本使用

1.  查看 package.json 中的 generate 脚本
2.  根据 docs 文件目录内容和添加 indexMd.json，自动化生成对应的 slider 对象 和 index.md
3.  注意：添加 --del 会删除 docs 中每个文件目录中的 index.md，不需要去掉即可

#### 参与贡献

发现文档中有任何不足，或者不理解的东西，欢迎提交 issues

#### 本项目地址：

- **gitee**: https://gitee.com/ma_zipeng/TS-Note
- **github**: https://github.com/sishen654/TS-Note
