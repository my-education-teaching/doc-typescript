# 高级编程

TS 高级编程学习 🎉, 需要了解的有:
1.  [01-实用程序类型](/advanced/01-实用程序类型.md)
2.  [02-TypeScript 备忘单](/advanced/02-TypeScript 备忘单.md)
3.  [03-装饰器](/advanced/03-装饰器.md)
4.  [04-声明合并](/advanced/04-声明合并.md)
5.  [05-enum](/advanced/05-enum.md)
6.  [06-迭代器和生成器](/advanced/06-迭代器和生成器.md)
7.  [07-JSX](/advanced/07-JSX.md)
8.  [08-混合模式](/advanced/08-混合模式.md)
9.  [09-ECMAScript 模块](/advanced/09-ECMAScript 模块.md)
10.  [10-TS中的模块](/advanced/10-TS中的模块.md)
11.  [11-模块解析](/advanced/11-模块解析.md)
12.  [12-命名空间](/advanced/12-命名空间.md)
13.  [13-Symbols](/advanced/13-Symbols.md)
14.  [14-三斜杠指令](/advanced/14-三斜杠指令.md)
15.  [15-类型兼容性](/advanced/15-类型兼容性.md)
16.  [16-类型推断](/advanced/16-类型推断.md)
# 基础语法

TS基础语法学习🎉, 需要了解的有:
1.  [01-TS简介](/base/01-TS简介.md)
2.  [02-基础数据类型](/base/02-基础数据类型.md)
3.  [03-类型范围缩小](/base/03-类型范围缩小.md)
4.  [04-函数详解](/base/04-函数详解.md)
5.  [05-对象类型](/base/05-对象类型.md)
6.  [06-类型操作](/base/06-类型操作.md)
7.  [07-类](/base/07-类.md)
8.  [08-模块](/base/08-模块.md)
# 配置文件及CLI命令

TS 配置文件属性大全以及CLI命令使用, 需要了解的有:
1.  [tsc CLI](/config/tsc CLI.md)
2.  [tsconfig配置项](/config/tsconfig配置项.md)
# 声明文件

TS 关于声明文件的创建和使用, 需要了解的有:
1.  [01-简介](/declaration/01-简介.md)
2.  [02-declare的作用](/declaration/02-declare的作用.md)
3.  [03-声明参考](/declaration/03-声明参考.md)
4.  [04-库的结构](/declaration/04-库的结构.md)
5.  [05-.d.ts模板](/declaration/05-.d.ts模板.md)
6.  [06-.d.ts声明位置](/declaration/06-.d.ts声明位置.md)
7.  [07-推荐编写](/declaration/07-推荐编写.md)
8.  [08-深潜](/declaration/08-深潜.md)
9.  [09-发布](/declaration/09-发布.md)
10.  [10-下载](/declaration/10-下载.md)
